import logo from './logo.svg';
import './App.css';
import MyHeading from './components/heading/heading';
import NavigationForm from './components/navigation/nav';
import Content1 from './components/content1/content1';
import Content2 from './components/content2/content2';
import Content3 from './components/content3/content3';
import Content4 from './components/content4/content4';
import Contact from './components/contact/contact';

function App() {
  return (<>
    <NavigationForm></NavigationForm>
    <MyHeading></MyHeading>
    <Content1></Content1>
    <Content2></Content2>
    <Content3></Content3>
    <Content4></Content4>
    <Contact></Contact>
    </>
  );
}

export default App;
