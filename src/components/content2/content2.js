import React, { useState } from "react";
import { Row, Col, Button } from "antd";
import "antd/dist/antd.css";
import "./index.css";
import { Carousel } from "antd";
import { EnvironmentOutlined } from "@ant-design/icons";

export default function Content2() {
  const onChange = (currentSlide) => {
    console.log(currentSlide);
  };
  return (
    <>
      <Row className="content2">
        <Col span={24} align="middle" style={{ marginTop: "5rem" }}>
          <h1 className="content1-heading">
            Bất động sản<span style={{ color: "#007BFF" }}> nổi bật</span>
          </h1>
        </Col>

        <Col md={{ span: 18, offset: 3 }}>
          <Carousel speed={0.5} autoplay >
            <div >
              <Row>
                <Col span={10} align="middle">
                  <Carousel>
                    <div>
                      <img src="https://bizweb.dktcdn.net/100/393/384/products/pd-34.jpg?v=1592542128000" className="img-fluid"></img>
                    </div>
                    <div>
                      <img src="https://bizweb.dktcdn.net/100/393/384/products/pd-35.jpg?v=1592542129000" className="img-fluid"></img>
                    </div>
                    <div>
                      <img src="https://bizweb.dktcdn.net/100/393/384/products/pd-36.jpg?v=1592542130000" className="img-fluid"></img>
                    </div>
                    <div>
                      <img src="https://bizweb.dktcdn.net/100/393/384/products/pd-37.jpg?v=1592542130000" className="img-fluid"></img>
                    </div>
                  </Carousel>
                </Col>
                <Col span={14} className="detail-project">
                  <div className="carousel-project">
                    <h1 className="name-carousel-project">Empire City</h1>
                    <h2 className="address-project">
                      Khu Đô thị Mới Thủ Thiêm
                    </h2>
                    <p className="description-project">
                      Empire City tọa lạc tại một vị trí đắc địa ven sông với
                      diện tích 14.6 ha tại khu Đô thị mới Thủ Thiêm, trung tâm
                      mới của TP.HCM. Dự án được quy hoạch sẽ bao gồm 3.000 căn
                      hộ cao cấp, văn phòng, khu bán lẻ và tòa tháp phức hợp cao
                      86 tầng.
                    </p>
                    <hr></hr>
                    <Button className="btn-here">
                      {" "}
                      <EnvironmentOutlined /> I'm here
                    </Button>
                    <Button className="btn-buy-now">Mua ngay</Button>
                  </div>
                </Col>
              </Row>
            </div>

            <div>
              <Row>
                <Col span={10} align="middle">
                  <Carousel>
                    <div>
                      <img src="https://bizweb.dktcdn.net/100/393/384/products/pd-41.jpg?v=1592552380000" className="img-fluid"></img>
                    </div>
                    <div>
                      <img src="https://bizweb.dktcdn.net/100/393/384/products/pd-19-5a18d4b1-5dab-45cd-b3bf-59df1f1b6cce.jpg?v=1592552381000" className="img-fluid"></img>
                    </div>
                    <div>
                      <img src="https://bizweb.dktcdn.net/100/393/384/products/pd-20-f59e8a15-e081-4e4a-b0ba-5aa7292fe36d.jpg?v=1592552382000" className="img-fluid"></img>
                    </div>
                    <div>
                      <img src="https://bizweb.dktcdn.net/100/393/384/products/pd-21-b503b4cb-0c31-4b77-8257-0c6eb200f0e8.jpg?v=1592552382000" className="img-fluid"></img>
                    </div>
                  </Carousel>
                </Col>
                <Col span={14} className="detail-project">
                  <div className="carousel-project">
                    <h1 className="name-carousel-project">ONE VERANDAH</h1>
                    <h2 className="address-project">Phường Thạnh Mỹ Lợi</h2>
                    <p className="description-project">
                      Verandah có nghĩa “Mái hiên nhà”, như tên gọi của mình, dự
                      án thiết kế hướng đến sự hòa hợp giữa các yếu tố thiên
                      nhiên và điều đó cũng thể hiện ở tên gọi của các tòa tháp:
                      Lumina (Ánh sáng), Ciel (Bầu trời trong xanh), Jardin
                      (Mảng xanh của cây cỏ), Viento (Gió). Sở hữu một vị thế vô
                      cùng nổi bật cạnh UBND Quận 2, liền kề dự án Đảo Kim
                      Cương, nằm trên 4 mặt tiền trục đường Bát Nàn – Nguyễn Văn
                      Kỉnh – Tạ Hiện – 103 Phường Thạnh Mỹ Lợi, Quận 2 và bên bờ
                      sông Sài Gòn, One Verandah được xem như một tuyệt tác bên
                      sông.
                    </p>
                    <hr></hr>
                    <Button className="btn-here">
                      {" "}
                      <EnvironmentOutlined /> I'm here
                    </Button>
                    <Button className="btn-buy-now">Mua ngay</Button>
                  </div>
                </Col>
              </Row>
            </div>

            <div>
              <Row>
                <Col span={10} align="middle">
                  <Carousel>
                    <div>
                      <img src="https://bizweb.dktcdn.net/100/393/384/products/pd-40.jpg?v=1592551737000" className="img-fluid"></img>
                    </div>
                    <div>
                      <img src="https://bizweb.dktcdn.net/100/393/384/products/pd-40.jpg?v=1592551737000" className="img-fluid"></img>
                    </div>
                    <div>
                      <img src="https://bizweb.dktcdn.net/100/393/384/products/pd-40.jpg?v=1592551737000" className="img-fluid"></img>
                    </div>
                    <div>
                      <img src="https://bizweb.dktcdn.net/100/393/384/products/pd-11-83abc1c2-232b-4f07-85ed-e5e81871b183.jpg?v=1592551739000"></img>
                    </div>
                  </Carousel>
                </Col>
                <Col span={14} className="detail-project">
                  <div className="carousel-project">
                    <h1 className="name-carousel-project">RICCA</h1>
                    <h2 className="address-project">Gò Cát</h2>
                    <p className="description-project">
                      Lấy cảm hứng từ tên địa danh Phú Hữu (có nghĩa là trù phú,
                      giàu có). RICCA mang nghĩa tương tự trong tiếng Ý, cũng có
                      thể coi là biến tấu của Rich (giàu có) trong tiếng Anh.
                      Đây cũng là kỳ vọng khi kiến tạo dự án: Xây dựng một cộng
                      đồng trẻ, tri thức, giàu có, thịnh vượng giữa lòng Quận 9,
                      đem lại cuộc sống thăng hoa cho cư dân tương lai.
                    </p>
                    <hr></hr>
                    <Button className="btn-here">
                      {" "}
                      <EnvironmentOutlined /> I'm here
                    </Button>
                    <Button className="btn-buy-now">Mua ngay</Button>
                  </div>
                </Col>
              </Row>
            </div>
          </Carousel>
        </Col>
      </Row>
    </>
  );
}
