import { Row, Col, Input , Button, Select } from "antd";
import { Option } from "antd/lib/mentions";
import React from "react";
import "./index.css";
export default function Contact() {
  return (
    <Row>
      <Col md={{ span: 18, offset: 3 }} align="middle">
        <h1 className="contact-heading">Liên hệ với chúng tôi</h1>
        <Row>
          <Col md={{ span: 6, offset: 1 }}>
            <Input placeholder="Họ và tên"></Input>
          </Col>
          <Col md={{ span: 6, offset: 1 }}>
            <Input placeholder="Last Name"></Input>
          </Col>
          <Col md={{ span: 8, offset: 1 }}>
            <Input placeholder="Email"></Input>
          </Col>
        </Row>
        <Row>
          <Col md={{ span: 6, offset: 1 }}>
            <Input placeholder="Địa chỉ"></Input>
          </Col>
          <Col md={{ span: 8, offset: 1 }}>
            <Select>
                <Option value='null'>Loại bất động sản quan tâm</Option>
            </Select>
          </Col>
          <Col md={{ span: 6, offset: 1 }}>
            <Button>Gửi</Button>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}
