import { Row, Col } from "antd";
import React from "react";
import './index.css'
export default function Content3() {
  return (
    <Row className="content3">
      <Col md={{span:10,offset:2}}>
        <h1 className="content3-heading">
          Sứ mệnh của chúng tôi là giúp mọi người tìm &{" "}
          <span style={{ color: "#007BFF" }}>
            tạo nên 1 không gian sống tốt hơn!
          </span>{" "}
        </h1>
        <p className="content3-des">
          Thành lập Công ty TNHH Dịch vụ và Xây dựng với vốn điều
          lệ ban đầu là 0,8 tỷ đồng và 10 nhân viên. Khi mới hoạt động,
          chuyên về môi giới các dự án bất động sản. Hiện tại, đã
          tăng vốn điều lệ lên 3.500 tỷ đồng với trên 3500 nhân viên.
        </p>
        <p className="content3-des">
          Trải qua chặng đường 17 năm phát triển, Tập đoàn  đã trở
          thành một trong những chủ đầu tư mang lại dấu ấn với sản phẩm đa dạng
          đáp ứng nhu cầu của thị trường.
        </p>
        <p className="content3-des">
          Tầm nhìn đến 2025: "Trở thành 1 trong 10 Tập đoàn kinh tế tư nhân lớn
          nhất Việt Nam" Tầm nhìn đến 2030: "Trở thành 1 trong 10 công ty phát
          triển Bất động sản tốt nhất Đông Nam Á"
        </p>
      </Col>
      <Col span={10}>
        <Row>
        <div className="content3-card-1">
            <div className="m-2">
                 <h4>Tìm kiếm thông tin dễ dàng</h4>
                <p>Không ngừng phát triển mạnh mẽ các hoạt động đầu tư xây dựng để nhanh chóng trở thành một trong những tập đoàn phát triển bất động sản hàng đầu</p>
            </div>
        </div>
        <div className="content3-card">
            <div className="m-2">
                 <h4>Tiết kiệm thời gian và chi phí</h4>
                <p>Tất cả các giải pháp mà  cung cấp đều được phân tích một cách chuyên sâu, hướng đến phục vụ và giải quyết những vướng mắc một cách nhanh chóng nhu cầu của khách hàng.</p>
            </div>
        </div>
        <div className="content3-card-1">
            <div className="m-2">
                 <h4>Đảm bảo quyền lợi khách hàng</h4>
                <p>Trải qua chặng đường 17 năm phát triển,  đã trở thành một trong những chủ đầu tư mang lại dấu ấn với sản phẩm đa dạng đáp ứng nhu cầu của thị trường</p>
            </div>
        </div>
        <div className="content3-card">
            <div className="m-2">
                 <h4 >Kết nối với nhà đầu tư</h4>
                <p>Là thương hiệu hàng đầu trên thị trường  luôn được các đối tác đánh giá cao về năng lực và uy tín kinh doanh. Đã trở thành đối tác tin cậy của hàng loạt công ty.</p>
            </div>
        </div>
        </Row>
      </Col>
      
    </Row>
  );
}
