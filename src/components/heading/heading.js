import { Button, Col, Input, Row } from 'antd';
import React from 'react'
import './index.css';
import { SearchOutlined } from '@ant-design/icons';

export default function MyHeading() {
  return (
    <div className='heading'>
        <Row>
            <Col md={{span:18,offset:4}} align='middle'>
                <h1 className='content'>Cách tiếp cận bất động sản nhanh nhất</h1>
            </Col>
            <Col md={{span:10,offset:4}} align='middle'>
                <Input placeholder='Nhập địa điểm hoặc từ khoá (Ví dụ:Vinhomes ... )' className='input-search'></Input>
                
            </Col>
            <Col md={{span:4,offset:1}}>
                <Button className='button-search'><SearchOutlined />Tìm kiếm</Button>
            </Col>
            <Col md={{span:18,offset:4}} align='middle' className='types'>
                <img src='//bizweb.dktcdn.net/100/393/384/themes/844266/assets/collection_1.png?1646297888986' className='icon-types'></img>
                <img src='//bizweb.dktcdn.net/100/393/384/themes/844266/assets/collection_2.png?1646297888986' className='icon-types'></img>
                <img src='//bizweb.dktcdn.net/100/393/384/themes/844266/assets/collection_3.png?1646297888986' className='icon-types'></img>
                <img src='//bizweb.dktcdn.net/100/393/384/themes/844266/assets/collection_4.png?1646297888986' className='icon-types'></img>
                <img src='//bizweb.dktcdn.net/100/393/384/themes/844266/assets/collection_5.png?1646297888986' className='icon-types'></img>
                <img src='//bizweb.dktcdn.net/100/393/384/themes/844266/assets/collection_6.png?1646297888986' className='icon-types'></img>
            </Col>
        </Row>
    </div>
  )
}
