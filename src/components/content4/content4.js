import { Col, Row } from 'antd'
import React from 'react'
import './index.css'
export default function Content4() {
  return (
    <Row className='content4'>
        <Col md={{span:20,offset:2}} align='middle'>
            <h1 className='content4-heading'>Các dự án đã triển khai tại một số thành phố</h1>
            <Row>
                <Col span={16}>
                    <Row className='m-2'>
                        <Col md={{span:9,offset:1}} style={{position:'relative'}}>
                            <img src='https://bizweb.dktcdn.net/100/393/384/themes/844266/assets/da_1.jpg?1646297888986' className='content3-img-fluid'></img>
                            <h4 style={{position:'absolute',
                        bottom:0,
                        fontSize:'1.5rem',
                        right:'1rem',
                        color:'white'}}>Hà Nội</h4>
                        </Col>
                        <Col md={{span:13,offset:1}} style={{position:'relative'}}>
                            <img src='//bizweb.dktcdn.net/100/393/384/themes/844266/assets/da_2.jpg?1646297888986' className='content3-img-fluid'></img>
                            <h4 style={{position:'absolute',
                        bottom:0,
                        fontSize:'1.5rem',
                        right:'1rem',
                        color:'white'}}>Đà Nẵng</h4>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={{span:9,offset:1}} style={{position:'relative'}}>
                            <img src='//bizweb.dktcdn.net/100/393/384/themes/844266/assets/da_3.jpg?1646297888986' className='content3-img-fluid'></img>
                            <h4 style={{position:'absolute',
                        bottom:0,
                        fontSize:'1.5rem',
                        right:'1rem',
                        color:'white'}}>Nghệ An</h4>
                        </Col>
                        <Col md={{span:13,offset:1}} style={{position:'relative'}}>
                            <img src='//bizweb.dktcdn.net/100/393/384/themes/844266/assets/da_4.jpg?1646297888986' className='content3-img-fluid'  ></img>
                            <h4 style={{position:'absolute',
                        bottom:0,
                        fontSize:'1.5rem',
                        right:'1rem',
                        color:'white'}}>Hải Phòng</h4>
                        </Col>
                    </Row>
                </Col>
                <Col md={{span:6,offset:1}} style={{position:'relative',marginBottom:'5rem'}}>
                    <img src='//bizweb.dktcdn.net/100/393/384/themes/844266/assets/da_5.jpg?1646297888986' className='content3-img-fluid' style={{height:'40rem'}}></img>
                    <h4 style={{position:'absolute',
                        bottom:0,
                        fontSize:'1.5rem',
                        right:'1rem',
                        color:'white',
                    }}>TP.Hồ Chí Minh</h4>
                </Col>
            </Row>
        </Col>
    </Row>

  )
}
