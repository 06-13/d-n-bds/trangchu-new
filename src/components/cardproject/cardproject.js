import { Button, Col, Row } from 'antd'
import React from 'react'
import "./index.css"
export default function CardProject({dataProject}) {
  return (
    <Row className='card' >
        <Col span={24} className="img-card">
          <img src={dataProject.photo} style={{width:'100%',height:'14rem'}}></img>
        </Col>
        <Col span={24} className="note-project">
            <Button className='button-note'>Hot</Button>
        </Col>
        <Col span={24} className="desription-project">
            {dataProject.description}
        </Col>
        <Col span={24} align='left' className='content-project'>
            <h1 className='name-project' style={{color:'blue'}}>{dataProject.name}</h1>
            <p>Địa chỉ : {dataProject.address}</p>
            <p>Số toà : {dataProject.numBlock}</p>
            <p>Số tầng : {dataProject.numFloors}</p>
            <p>Diện tích : {dataProject.acreage} m2</p>
            <p className='price'>15.15 tỷ</p>
        </Col>
        <Button className='button-buy'>Mua ngay</Button>
    </Row>
  )
}
