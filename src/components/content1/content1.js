import { Col, Row } from "antd";
import React, { useEffect, useState } from "react";
import "./index.css";
import CardProject from "../cardproject/cardproject";
import axios from 'axios';

export default function Content1() {
  const [projects, setProjects] = useState([]);
  useEffect(() => {
    loadData();
  });
  const loadData = () => {
    axios
      .get("http://localhost:8080/BDS/project")
      .then((data) => setProjects(data.data))
  };
  return (
    <>
      <Row>
        <Col span={24} align="middle" style={{ marginTop: "5rem" }}>
          <h1 className="content1-heading">
            Bất động sản<span style={{ color: "#007BFF" }}> cho bán</span>
          </h1>
        </Col>
      </Row>
      <Row>
        <Col md={{ span: 20, offset: 2 }}>
          <Row>
                {projects.map((info) => {
                    return (
                    <Col align="middle" md={{ span: 5, offset: 1 }} style={{marginTop:'2rem'}} key={info.id}>
                        <CardProject dataProject={info}></CardProject>
                    </Col>
                    )
                })}
          </Row>
        </Col>
      </Row>
    </>
  );
}
