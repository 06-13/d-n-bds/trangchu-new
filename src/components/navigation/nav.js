import React from 'react';
import 'antd/dist/antd.css';
import './index.css';
import { PhoneOutlined } from '@ant-design/icons';
import { Button, Menu } from 'antd';
import { useState } from 'react';

const items = [
  {
    label: <img src='//bizweb.dktcdn.net/100/393/384/themes/844266/assets/logo.png?1646297888986'></img>,
    key: 'icon',
  },
  {
    label: 'Trang chủ',
    key: 'home',
  },
  {
    label: 'Giới thiệu',
    key: 'introduce',
  },
  {
    label: 'Sản phẩm',
    key: 'products',
    children: [
      {
        type: 'Option',
        label: 'Căn hộ chung cư',
      },
      {
        type: 'Option',
        label: 'Nhà riêng',
      },
      {
        type: 'Option',
        label: 'Biệt thự liền kề',
      },
      {
        type: 'Option',
        label: 'Nhà mặt phố',
      },
      {
        type: 'Option',
        label: 'Đất nền dự án',
      },
      {
        type: 'Option',
        label: 'Trang trại, khu nghỉ dưỡng',
      },
      {
        type: 'Option',
        label: 'Kho, nhà xưởng',
      },
    ],
  },
  {
    label: 'Tin tức',
    key: 'news',
  },
  {
    label: 'Liên hệ',
    key: 'contact',
  },
  {
    label: <><Button className='button-contact'><PhoneOutlined className='icon-contact'/> Liên hệ tư vấn</Button></>,
    key: 'contactphone',
  },
];

const NavigationForm = () => {
  const [current, setCurrent] = useState('home');

  const onClick = (e) => {
    setCurrent(e.key);
  };
  return <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} />;
};

export default NavigationForm;